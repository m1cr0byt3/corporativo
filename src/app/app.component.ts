import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav,ModalController } from 'ionic-angular';

import { Login } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Splash } from '../pages/splash/splash';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = Login;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public modalCtrl: ModalController,
    private translate: TranslateService
  ) {
    this.initTranslate()
    this.initializeApp();
  }


  initTranslate() {
    this.translate.setDefaultLang('es');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      let splash = this.modalCtrl.create(Splash);
      splash.present();

    });
  }
}
