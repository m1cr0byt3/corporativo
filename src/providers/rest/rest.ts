import  { HttpClient,HttpHeaders } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core'; 

@Injectable()
export class RestProvider {

  baseUrl = "http://localhost:8100/api";
  messageError;

  headers: Headers;
  options: RequestOptions;

  constructor(
    public http: HttpClient, 
    public translate:TranslateService,
    public alert:AlertController) {

      this.headers = new Headers;
      this.headers.append("Accept","application/json");
      this.headers.append("Content-Type","application/json");
      this.options = new RequestOptions;
      this.options.headers = this.headers;
      this.options.withCredentials = true;



      translate.get('LOGIN_ERROR_NETWORK').subscribe(
        value => {
          // value is our translated string
          this.messageError = value;
        }
      )

      
    
  }

  private errorNetwork(){
    let alert = this.alert.create({
      title: 'Error',
      subTitle: this.messageError,
      buttons: ['Ok']
    });
    alert.present();
  }

  //All endpoints to consume
  //Cors está activado desde el backend porque le mando la el Across
  enterprises(){
    /* 
      Te da un listado de empresas
    */

    return new Promise(resolve => {
      this.http.get(this.baseUrl+'/api/enterprises',{ withCredentials: true}).subscribe(data => {
          resolve(data);
      }, err => {
        this.errorNetwork();
      });
    });
  }


  recoverPassword(email){
    /* 
      Te manda un correo para recuperar el password
    */
    return new Promise(resolve => {
      this.http.post(this.baseUrl+'/api/recover/password', {email:email})
      .subscribe(data => {
          resolve(data);
      }, error => {
        this.errorNetwork();
      });
    });
  }


  login(login){
    /*
      Manda el usuario y el password para hacer el inicio de sesión    
    */
    return new Promise(resolve => {
      this.options.body = login;
      this.http.post(this.baseUrl+'/api/login',this.options)
      .subscribe(data => {
          resolve(data);
      }, error => {
        this.errorNetwork();
      });
    });
  }

  isLogedin(){
    /* 
    Sirve para checar si la sesión está activa
    */
    ///api/is/logedin GET
    return new Promise(resolve => {
      this.http.get(this.baseUrl+'/api/is/logedin').subscribe(data => {
          resolve(data);
      }, err => {
        this.errorNetwork();
      });
    });
  }

}
