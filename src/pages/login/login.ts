import { Component } from '@angular/core';
import { AlertController,NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core'; 

import { RestProvider } from '../../providers/rest/rest';

import { Forgotten } from '../forgotten/forgotten';
import { Dashboard } from '../dashboard/dashboard';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login{
    messageError: string;
    alertError: any;
    login = {}
  constructor(
      public alertCtrl: AlertController,
      public splashScreen: SplashScreen, 
      public api: RestProvider,
      public translate: TranslateService,
      public navCtrl:NavController) {
          this.translate.get('LOGIN_ERROR').subscribe(
            value => {
                this.messageError = value;
            }
          )
          this.alertError = alertCtrl;
          
  }
  
  //Load Splash
  ionViewDidEnter() {
    this.splashScreen.hide();
  }

  //Event to Login
  doLogin() {
    this.api.login(this.login)
    .then(data => {
        this.endpoint(data);
    });
  }

  endpoint(data){
    let alert = this.alertError.create({
        title: 'Error',
        subTitle: this.messageError,
        buttons: ['Ok']
      });

    if(!data.error){
        this.navCtrl.push(Dashboard);
    }else{
        alert.present();
    }
  }

  viewForgotten(){
    this.navCtrl.push(Forgotten);
  }

 
}