import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core'; 
import { RestProvider } from '../../providers/rest/rest';
import { Login } from '../login/login';

@Component({
  selector: 'forgotten-login',
  templateUrl: 'forgotten.html',
})
export class Forgotten{
    
    messageError: string;
    alertError: any;
    email;
  constructor(
      public alertCtrl: AlertController,
      public api: RestProvider,
      public translate: TranslateService,
      public navCtrl:NavController) {
          this.translate.get('COMMON_ERROR_NETWORK').subscribe(
            value => {
                this.messageError = value;
            }
          )
          this.alertError = alertCtrl;
          
  }

makeAlert(message){
    return this.alertCtrl.create({
        title: 'Éxito',
        subTitle: message,
        buttons: ['Ok']
      });
  }

  doForgotten(){
    

    this.api.recoverPassword(this.email)
    .then(data => {
        this.endpoint(data,function(alert){
              alert.present();
        });
    });
  }

  endpoint(data,callBack){
    let alert = this.alertError.create({
        title: 'Error',
        subTitle: this.messageError,
        buttons: ['Ok']
      });

    if(!data.error){
        callBack(this.makeAlert(data.message));
        this.back();
    }else{
        alert.present();
    }
  }

  back(){
    this.navCtrl.push(Login);
  }

 
}