import { Component } from '@angular/core';
import { ViewController,NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../../providers/rest/rest';
import { Dashboard } from '../dashboard/dashboard';
 
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class Splash {
 
  constructor(
    public viewCtrl: ViewController, 
    public splashScreen: SplashScreen,
    public api:RestProvider,
    public navCtrl:NavController) {
 
  }
 
  ionViewDidEnter() {

    this.api.isLogedin()
    .then(data => {
      this.endpoint(data)
    });
 
    this.splashScreen.hide();
 
    this.goLogin();
 
  }

  endpoint(data){
    if(!data.error){
      this.navCtrl.push(Dashboard);
    }else{
      this.goLogin();
    }

    
  }

  private goLogin(){
    setTimeout(() => {
      this.viewCtrl.dismiss();
    }, 5000);
  }
}