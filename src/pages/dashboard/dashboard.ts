import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { TranslateService } from '@ngx-translate/core'; 


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class Dashboard {
  loadingMessage;

  request = {error: false, data: [], message: ""};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api:RestProvider,
    public loadingCtrl:LoadingController,
    public alertCtrl:AlertController,
    public translate:TranslateService) {
      this.translate.get('LOADING').subscribe(
        value => {
            this.loadingMessage = value;
        }
      )
  }

  private alertError(message){
      return this.alertCtrl.create({
          title: 'Error',
          subTitle: message,
          buttons: ['Ok']
        });
  }

  ionViewDidLoad() {
    const loader = this.loadingCtrl.create({
      content: "Cargando..."
    });
    loader.present();
    this.api.enterprises()
    .then(data => {
      
      if(!data["error"]){
        console.table(data["data"]);
      }else{
        this.alertError(data["message"]);
      }
      loader.dismiss();
    });

  setTimeout(() => {
    loader.dismiss();
  }, 4000);

  
  }

}
